<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @copyright   Copyright (c) 2014 X.commerce, Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magento\Bannerslider\Block;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;
class Slideshow extends Template
{
    public function __construct(
        Context $context,
		\Magento\Bannerslider\Model\BannerFactory $bannerFactory,
		\Magento\Framework\Filesystem $fileSystem,
        array $data = []
    ) {
		$this->_fileSystem = $fileSystem;
		$this->_bannerFactory = $bannerFactory;
        parent::__construct($context, $data);
    }
	public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
	public function getBaseJs($fileName){

		return $this->_storeManager->getStore()->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
            ).'bannerslider/js/'.$fileName;
	}
	public function getBannerData(){
		$banners = $this->_bannerFactory->create()
						->getCollection()
						->addFieldToFilter('status',1);
		foreach ($banners as $banner){
                $result['banners'][] = $banner->getData();
		} 
		return $result;
	}
	public function getBannerImage($imageName) {
	
        $mediaDirectory = $this->_storeManager->getStore()->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
            );
		return $mediaDirectory.'bannerslider/images'.$imageName;
    }
}
